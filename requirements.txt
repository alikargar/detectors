astroid @ file:///tmp/build/80754af9/astroid_1592495881661/work
certifi==2020.6.20
chardet==3.0.4
emoji-extractor==1.0.17
gender-guesser==0.4.0
idna==2.10
isort @ file:///tmp/build/80754af9/isort_1602603989581/work
jdatetime==3.6.2
langdetect==1.0.8
lazy-object-proxy==1.4.3
mccabe==0.6.1
numpy==1.19.4
opencv-python==4.4.0.46
pylint @ file:///tmp/build/80754af9/pylint_1598624038450/work
pysolr==3.9.0
requests==2.24.0
six==1.15.0
toml @ file:///tmp/build/80754af9/toml_1592853716807/work
typed-ast==1.4.1
urllib3==1.25.11
wrapt==1.11.2
xlrd==1.2.0
xlutils==2.0.0
xlwt==1.3.0
